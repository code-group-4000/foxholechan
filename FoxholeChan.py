#!/usr/bin/env python
import PySimpleGUI as sg
import requests
import re#eeeeeeeeeeee
import json
import glob
import PIL
from PIL import Image
from PIL import UnidentifiedImageError
from pprint import pprint#not using this. Just want to import it.
from datetime import datetime as dt#Not using this either. Just importing it is all. Haha

#### CODE FUNCTION FOR BRINGING OUT THE DATA ####
def decode(filename):
#    img = input("Enter image name(with extension) : ")
    try:
        image = Image.open(filename  + '.png', 'r')
    except PIL.UnidentifiedImageError:
        print(str(filename) + ' had an error... skipping')
        return "ERROR"
    data = ''
    imgdata = iter(image.getdata())
 
    while (True):
        pixels = [value for value in imgdata.__next__()[:3] +
                                imgdata.__next__()[:3] +
                                imgdata.__next__()[:3]]
 
        # string of binary data
        binstr = ''
 
        for i in pixels[:8]:
            if (i % 2 == 0):
                binstr += '0'
            else:
                binstr += '1'
 
        data += chr(int(binstr, 2))
        if (pixels[-1] % 2 != 0):
            return data


############################ CODE FOR ENCODING THE DATA #####################################
# Convert encoding data into 8-bit binary
# form using ASCII value of characters
def genData(data):
 
        # list of binary codes
        # of given data
        newd = []
 
        for i in data:
            newd.append(format(ord(i), '08b'))
        return newd
 
# Pixels are modified according to the
# 8-bit binary data and finally returned
def modPix(pix, data):
 
    datalist = genData(data)
    lendata = len(datalist)
    imdata = iter(pix)
 
    for i in range(lendata):
 
        # Extracting 3 pixels at a time
        pix = [value for value in imdata.__next__()[:3] +
                                imdata.__next__()[:3] +
                                imdata.__next__()[:3]]
 
        # Pixel value should be made
        # odd for 1 and even for 0
        for j in range(0, 8):
            if (datalist[i][j] == '0' and pix[j]% 2 != 0):
                pix[j] -= 1
 
            elif (datalist[i][j] == '1' and pix[j] % 2 == 0):
                if(pix[j] != 0):
                    pix[j] -= 1
                else:
                    pix[j] += 1
                # pix[j] -= 1
 
        # Eighth pixel of every set tells
        # whether to stop ot read further.
        # 0 means keep reading; 1 means thec
        # message is over.
        if (i == lendata - 1):
            if (pix[-1] % 2 == 0):
                if(pix[-1] != 0):
                    pix[-1] -= 1
                else:
                    pix[-1] += 1
 
        else:
            if (pix[-1] % 2 != 0):
                pix[-1] -= 1
 
        pix = tuple(pix)
        yield pix[0:3]
        yield pix[3:6]
        yield pix[6:9]
 
def encode_enc(newimg, data):
    w = newimg.size[0]
    (x, y) = (0, 0)
 
    for pixel in modPix(newimg.getdata(), data):
 
        # Putting modified pixels in the new image
        newimg.putpixel((x, y), pixel)
        if (x == w - 1):
            x = 0
            y += 1
        else:
            x += 1
 
# Encode data into image
def encode(img, name, comment):
    #img = input("Enter image name(with extension) : ")
    image = Image.open(img, 'r')
    new_img_name = str("output" + img)
    #new_img_name = input("Enter the name of new image(with extension) : ")
    data = ""
    #name = input("What is your name or leave blank for anonymous? ") or "Anonymous"
    #comment = input("What is your comment?") or "Comment left blank."
    data += "\n \n" + "Filename: " + str(new_img_name) + "\n" + "Name: " + str(name) +"\n"+ "Comment: \n" + str(comment)
    if (len(data) == 0):
        raise ValueError('Data is empty')
 
    newimg = image.copy()
    encode_enc(newimg, data)
 
    
    newimg.save(new_img_name, str(new_img_name.split(".")[1].upper()))
    print("Image saved to: " + new_img_name)
############
    
def findfrens():
    PossibleThreads = {}
    listofallpngonboard = []
    board = 'k'#Hard coded for now. can allow for user input later
    r = requests.get('https://a.4cdn.org/k/catalog.json')
    r = r.json()
    for n in range(9):
        for i in range(15):#iterate through all threads on a page
            if 'png' in str((r[n]['threads'][i]['ext'])):
                #print("png detected")
                listofallpngonboard.append(str((r[n]['threads'][i]['tim'])))
                PossibleThreads[str((r[n]['threads'][i]['tim']))] = (str((r[n]['threads'][i]['no'])))
            else:
                None
        else:
            None
    else:
        None


    print(listofallpngonboard)
    #now we have a list of all .pngs in the thread. we should check if we already have them, then download the images. To get this working as fast as possible , going to skip checking
    #TO DO CHECK FOR DUPLICATES BEFORE DOWNLOADING ### ADD THIS CODE HERE
    # IF YOU SEE THIS WARNING YOU SHOULD PROBABLY FIX THIS. It doesn't break anything but uses bandwidth
    threads = ""
    for i in range(len(listofallpngonboard)):
        try:############THIS TRY IS COVERING UP THE BUG OF .PNG BEING MENTIONED IN COMMENTS 
            with open(listofallpngonboard[i]+'.png', 'wb') as f:
                f.write(requests.get('https://i.4cdn.org/' + board +'/' + listofallpngonboard[i] + '.png').content)
                if "Filename" in str(decode(str(listofallpngonboard[i]))):#This cuts out false "decodes" of posts that aren't posts.
                    #print("WHAT THE HELL " + str(decode(str(listofallpngonboard[i]))))
                    #print(PossibleThreads)
                    threads += PossibleThreads[str(listofallpngonboard[i])]
                    #print("Threads: " + threads)
##                    posts += str(decode(str(listofallpngonboard[i])))### Security issue. Should probably sanatize this.
                else:
                    null
        except:
            None
##            else:
##                None
    return threads






def RetrievePosts(URLtoParse):
    if len(URLtoParse) < 5:#check to see if it's an empty box.
        return "Invalid Entry"
    #TO DO
    #Sanity checking and sanitization of user input
    #Add function to scan all boards if CanGen is banned
    ThreadNumber = URLtoParse.rsplit("thread/")[1]
    board = URLtoParse.rsplit("thread/")[0][-2:-1]

    #If function is added to scan all boards, add code here for scanning the board json for new threads
    listofallpnginthread = []
    
    r = requests.get('https://a.4cdn.org/' + board + '/thread/' + ThreadNumber +'.json')
    r = r.json()
    total_posts = int((r['posts'][0]['replies'])) #find out how many replies are in thread.
    for i in range(total_posts+1):
        try:
            if 'png' in str((r['posts'][i]['ext'])):#check the extension
                listofallpnginthread.append(str((r['posts'][i]['tim'])))#posts will contain all the "real" filenames.
            else:
                None
        except KeyError:
            None

    print(listofallpnginthread)
    #now we have a list of all .pngs in the thread. we should check if we already have them, then download the images. To get this working as fast as possible , going to skip checking
    #TO DO CHECK FOR DUPLICATES BEFORE DOWNLOADING ### ADD THIS CODE HERE
    # IF YOU SEE THIS WARNING YOU SHOULD PROBABLY FIX THIS. It doesn't break anything but uses bandwidth
    posts = ""
    for i in range(len(listofallpnginthread)):
        try:############THIS TRY IS COVERING UP THE BUG OF .PNG BEING MENTIONED IN COMMENTS 
            with open(listofallpnginthread[i]+'.png', 'wb') as f:
                f.write(requests.get('https://i.4cdn.org/' + board +'/' + listofallpnginthread[i] + '.png').content)
        #       image = Image.open(listofallpnginthread[i] + '.png', 'r')
    ##            if len(str(decode(str(listofallpnginthread[i])))) > 70:
                if "Filename" in str(decode(str(listofallpnginthread[i]))):#This cuts out false "decodes" of posts that aren't posts.
                    posts += str(decode(str(listofallpnginthread[i])))### Security issue. Should probably sanatize this.
                else:
                    null
        except:
            None
##            else:
##                None
    return posts
     #       return str(decode(str(listofallpnginthread[i])))


#######################################################################
# Main Function
def main():

##'''
##A simple send/response chat window.  Add call to your send-routine and print the response
##If async responses can come in, then will need to use a different design that uses PySimpleGUI async design pattern
##'''

    sg.theme('GreenTan') # give our window a spiffy set of colors

    right_click_menu = ['', ['Paste']]#copy paste menu
    threadtocheck_key = '-threadtocheck-'#copy paste menu
    text_image = '''                           .===;========.__, 
                           (\__)___________| 
     L__________________,--,--/ /-,-,-\ \-,   ________          Welcome to FoxholeChan.
=====)o o o o ======== )) ____,===,___""" "7_/_,_,_,_,'---,-,   To start, select a thread that may have
     `--._,_,_,-,--,--'' (____| _ \___\oo ; ; ; ; ; ;_____ T|   hidden messages inside and check_for_posts.
              `-'--'-/_,-------| ) ___--,__,------._ \__  |I| 
                       \==----/   \\ )\--\_         `-._`-'I|   To upload, stick a png in the same directory
                       /=[JW]/     `"==.- -\            `-.L|   as this program enter its name and your 
                      /==---/           \- -\                   post will be hidden inside the picture.
                      '-.__/             \__7  In the event Canada General is ever B&, create a post using this tool. \n And create a new thread with that image. This will alow others to easily find you and communicate using this program.'''
    layout = [[sg.Text('Incoming posts arrive here.', size=(40, 1))],
              [sg.Multiline(size=(110, 20),
                            reroute_stdout=True,
                            reroute_stderr=True,
                            autoscroll = True,
                            font=('Helvetica 10'))],
              [sg.Text('Which thread to check?')],
              [sg.Multiline(size=(50, 1), enter_submits=False, key='-threadtocheck-', right_click_menu=right_click_menu, do_not_clear=False),
               sg.Button('CANGEN_IS_BANNED_EMERGENCY_FIND_FRENS_BUTTON', button_color=(sg.YELLOWS[0], sg.GREENS[0]))],
              [sg.Text('Username: ')],#name
              [sg.Multiline(size=(50, 1), enter_submits=False, key='-name-', do_not_clear=False)],
              [sg.Text('Comment: ')],#comment
              [sg.Multiline(size=(50, 1), enter_submits=False, key='-comment-', do_not_clear=False)],
              [sg.Text('File Name of .png (including .png): ')],#img
              [sg.Multiline(size=(50, 1), enter_submits=False, key='-img-', do_not_clear=False),
               sg.Button('CHECK_FOR_NEW_POSTS', button_color=(sg.YELLOWS[0], sg.BLUES[0]), bind_return_key=True),
               sg.Button('CREATE_POST', button_color=(sg.YELLOWS[0], sg.GREENS[0]))]]
              #,
               #sg.Button('TURTLE', button_color=(sg.YELLOWS[0], sg.GREENS[0]))]]


    window = sg.Window('Foxhole Chan', layout, font=('Helvetica', ' 13'), default_button_element_size=(8,2), use_default_focus=False, finalize = True)
    threadcheckline:sg.Multiline = window[threadtocheck_key]#copy paste menu
    print(text_image)


    while True:     # The Event Loop
        event, value = window.read()
                
        #if event in (sg.WIN_CLOSED, 'EXIT'):
        if event in (sg.WIN_CLOSED, 'EXIT'):            # quit if X
            break
    ##    if event == 'TURTLE':
    ##        print("TURTLE")
        if event == 'CREATE_POST':
            print("Creating Image")
            img = value['-img-'].rstrip()
            #print(str(img))
            name = value['-name-'].rstrip()
            #print(str(name))
            comment = value['-comment-'].rstrip()
            
            encode(img, name, comment)
            print("... Done ... \n")
        if event == 'CHECK_FOR_NEW_POSTS':
            #query = value['-QUERY-'].rstrip()
            threadtocheck = value['-threadtocheck-'].rstrip()
            # EXECUTE YOUR COMMAND HERE
            print('The following posts were retrieved: ' + RetrievePosts(threadtocheck))
            #print('The command you entered was {}'.format(query), flush=True)
        if event == 'CANGEN_IS_BANNED_EMERGENCY_FIND_FRENS_BUTTON':
            thread = findfrens()
            if (len(thread)) >= 8:
                print('Friend Thread Found At: https://boards.4channel.org/k/thread/' + thread)
            else:
                print('No frens found :(.... Make a post!')
            
        #Code below for right click menu
        if event == 'Paste':
            threadcheckline.Widget.insert(sg.tk.INSERT, window.TKroot.clipboard_get())

    window.close()

# Driver Code
if __name__ == '__main__' :
 
    # Calling main function
    main()



# FoxholeChan

We have fun here.

## Description
You can post and make comments/ have conversations with people inside .png images inside threads on 4chan.
This projects goal is to eventually host an image boards inside other image boards by means of parasitic file storage (hiding the data in .pngs)

## Visuals

![image.png](./image.png)
## Installation
You can run a build.  The standalone windows build is rarely updated. Use at your own risk.
Or run the source using python3:
Install Python3.
Install the required packages (run this in shell or command line):
pip install -r requirements.txt
then:
python FoxholeChan.py


## Usage
Place your .png file in the same directory as the .py or binary.
Run program.
Insert your hidden comment in to the image.
Upload the image.
???? Share dank memes the jannies can't see.

## Support
Good luck.

## Roadmap
Eventually there will be a web based interface. Probably with flask. So on your side it will look like a normal image board. While the networking will happen in the back end by posting images.
Oh, should probably have some sort of security. Untill then you will probably want to run this in a VM.

## Contributing
If you want to pitch in, that's great. I'll do my best to add them.
Just make sure you don't add any bugs and try and comment.

## Authors and acknowledgment
Anon.

## License
        DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
                    Version 2, December 2004 

 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net> 

 Everyone is permitted to copy and distribute verbatim or modified 
 copies of this license document, and changing it is allowed as long 
 as the name is changed. 

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION 

  0. You just DO WHAT THE FUCK YOU WANT TO.
  1. Don't sue me. Use at your own risk.
## Project status
Working on it. Feel free to pitch in.
